'''
Created on 9 May 2018

@author: Arijit
'''

from hyper import HTTPConnection
from hyper import HTTP20Connection
from urlparse import urlparse



def make_request(http_method, request_url, request_body):
    url_parse_result = urlparse(request_url)
    
    connection = HTTPConnection(url_parse_result.hostname)
    
        
    if(url_parse_result.scheme == "https"):
        connection = HTTP20Connection(url_parse_result.hostname, secure=True)
    else:
        connection = HTTP20Connection(url_parse_result.hostname)
    
    '''    
    if(url_parse_result.scheme == "https"):
        connection = HTTPConnection(url_parse_result.hostname)
    else:
        connection = HTTP20Connection(url_parse_result.hostname)
    '''
    
    if(http_method == "GET"):
        connection.request(method = http_method, url = url_parse_result.path)
    elif(http_method == "HEAD"):
        # TODO HEAD Operation
        # connection.request(method, url_parse_result.path, body = body)
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    elif(http_method == "POST"):
        # TODO POST Operation
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    elif(http_method == "PUT"):
        # TODO PUT Operation
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    elif(http_method == "DETELE"):
        # TODO DETELE Operation
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    elif(http_method == "CONNECT"):
        # TODO CONNECT Operation
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    elif(http_method == "OPTIONS"):
        # TODO OPTIONS Operation
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    elif(http_method == "TRACE"):
        # TODO TRACE Operation
        connection.request(method = http_method, url = url_parse_result.path, body = request_body)
    else:
        #TODO : Throw exception here
        # print "HTTP method not proper"
        raise Exception('HTTP method not proper')
    
    response = connection.get_response()
    
    return response