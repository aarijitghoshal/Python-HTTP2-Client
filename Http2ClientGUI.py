'''
Created on 12 May 2018

@author: Arijit
'''

from Tkinter import *
from ScrolledText import ScrolledText
from Http2Client import make_request


top = Tk()
top.wm_state("zoomed")
top.resizable(width=False, height=False)
frame = Frame(top)
frame.pack()

url = StringVar(frame)
method = StringVar(frame)
no_of_requests = IntVar(frame)
data = ""


def send_request():
    data = data_textarea.get(1.0, END)
    requests_count = no_of_requests.get()
    
    output_textarea.config(state=NORMAL)
    output_textarea.delete(1.0, END)
    
    while(requests_count > 0):
        try:
            response = make_request(method.get(), url.get(), data)
            
            output_textarea.insert(END, "-------status-------\n")
            output_textarea.insert(END, str(response.status) + " " + response.reason)
            output_textarea.insert(END, "\n-------status-------\n\n")
            
            output_textarea.insert(END, "-------headers-------\n")

            for key in response.headers.keys():
                output_textarea.insert(END, key + " : " + str(response.headers[key]) + "\n")
             
            output_textarea.insert(END, "-------headers-------\n\n")
            
            output_textarea.insert(END, "-------body-------\n")
            output_textarea.insert(END, response.read())
            output_textarea.insert(END, "\n-------body-------\n\n")
            
            response.close()
            
        except Exception as e:
            output_textarea.insert(END, "Exception: \n")
            output_textarea.insert(END, str(e))
        
        output_textarea.update_idletasks()
        output_textarea.see(END)
        requests_count = requests_count - 1
        
    output_textarea.config(state=DISABLED)


method_label = Label(frame, text="Method: ")
method_label.grid(row=0, column=0, padx = 10, pady = 10)

choices = { "GET", "HEAD", "POST", "PUT", "PATCH", "DETELE", "CONNECT", "OPTIONS", "TRACE" }
method.set("GET")
method_dropdown = OptionMenu(frame, method, *choices)
method_dropdown.config(width = 10)
method_dropdown.grid(row = 0, column =1)

url_label = Label(frame, text="URL")
url_label.grid(row=0, column=2)

url_textbox = Entry(frame, textvariable = url, width = 100)
url_textbox.grid(row=0, column=3, ipady = 5)

no_of_requests_label = Label(frame, text="No. of requests: ")
no_of_requests_label.grid(row=0, column=4)

no_of_requests_spinner = Spinbox(frame, from_=1, to=10, width=10, textvariable = no_of_requests)
no_of_requests_spinner.grid(row=0, column=5, ipady=3)

send_button = Button(frame, text="Send", command = send_request)
send_button.grid(row=0, column=6, ipadx = 10, ipady = 1)

data_textarea_label = Label(frame, text="Request body: ")
data_textarea_label.grid(row=1, column=0)

data_textarea = ScrolledText(frame, height = 8, width=150, padx=10, pady=10, wrap=WORD);
data_textarea.grid(row=2, column=0,padx = 10, pady = 10, columnspan = 7)

output_textarea_label = Label(frame, text="Response: ")
output_textarea_label.grid(row=3, column=0, padx = 10)

output_textarea = ScrolledText(frame, height = 27, width=150, padx=10, pady=10, wrap=WORD, state=DISABLED);
output_textarea.grid(row=4, column=0,padx = 10, pady = 10, columnspan = 7)

top.mainloop()