About
-
This is a free and open source Python GUI client for HTTP/2.


Installation Guide
-
1. This project is tested on Windows 8 - 64 bit.
2. Install the python-2.7.11.msi in C:\ drive.
3. Set path for python and pip.
4. Open the command prompt and use "pip install hyper".
5. Run the Http2ClientGUI.py


Sample Request
-
GET https://jsonplaceholder.typicode.com/posts

POST https://jsonplaceholder.typicode.com/posts {title: 'foo',body: 'bar',userId: 1}

PUT https://jsonplaceholder.typicode.com/posts/1 {id: 1, title: 'foo',body: 'bar',userId: 1}

PATCH https://jsonplaceholder.typicode.com/posts/1 {title: 'foo'}

DELETE https://jsonplaceholder.typicode.com/posts/1


Developer: Arijit Ghoshal
Contact: aarijitghoshal@gmail.com
Greetings. Have fun.